#!/bin/bash
cd || exit
git clone --depth 1 https://github.com/daniestevez/gr-satellites.git
cp gr-satellites/python/satyaml/* /usr/lib/python3/dist-packages/satellites/satyaml/
rm -rf gr-satellites /tmp/.satnogs/grsat_list.*
git clone https://gitlab.com/space-education-development/soniks/soniks-satyaml.git
cp soniks-satyaml/satyaml/* /usr/lib/python3/dist-packages/satellites/satyaml/
rm -rf soniks-satyaml /tmp/.satnogs/grsat_list.*
exec "$@"


