import os, sys
import shutil

def find_files(directory, file_type):
    files = []
    for file in os.listdir(directory):
        if file.endswith(f"{file_type}") and not file.endswith(f"raw.png"):
            files.append(file)
    return files

def files_copy(files_array, old_dir, sat_id, time_st):
    for name in range(len(files_array)):
        new_loc=f"/tmp/.satnogs/data/data_{sat_id}_{time_st}_{files_array[name]}"
        old_loc=f"{old_dir}/{files_array[name]}"
        print("Runngnig tranfer")
        shutil.move(old_loc,new_loc)
    return

if(__name__=="__main__"):
    png_files_folder=sys.argv[1]
    sat_id=sys.argv[2]
    time_st=sys.argv[3]
    file_names_arr=find_files(directory=f"{png_files_folder}",file_type=".png")
    files_copy(files_array=file_names_arr, old_dir=f"{png_files_folder}", sat_id=f"{sat_id}", time_st=f"{time_st}")
