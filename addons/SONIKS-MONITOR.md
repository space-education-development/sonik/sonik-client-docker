# Запуск soniks-monitor на компьютере с запущенным soniks-client

## Инструкция

Конфигурация для монитора (в поле soniks_id укажите номер вашей станции на сайте sonik.space):
```
[[stations]]
soniks_id = XX
local = true
[ui]
db_min = -100.0
db_max = 0.0

job_update_interval = 300
ground_track_num = 3
sat_footprint = true
spectrum_plot = true
waterfall = true
```
Отредактируйте конфигурацию монитора, скопируйте ее и, запустив следующую команду из папки станции на рабочем компьютере клиента, вставьте её в консоль:<br>
`docker-compose exec satnogs_client bash -c "mkdir -p ~/.config/satnogs-monitor/ && cat > ~/.config/satnogs-monitor/config.toml"`<br>
После того, как вы вставили содержимое, нажмите Ctrl-D<br>
Запустите монитор, вызвав команду `docker-compose exec satnogs_client satnogs-monitor`

