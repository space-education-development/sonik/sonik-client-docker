#!/bin/bash
export DOCKER_BUILDKIT=1
TAG="1.2.8-addons"
#REPO_ROOT="registry.gitlab.com/space-education-development/soniks"
REPO_ROOT="sonikspace"
SATNOGS_IMAGE_TAG="1.2.7"
ARGS="
    --build-arg SATNOGS_IMAGE_TAG=${SATNOGS_IMAGE_TAG} \
    --build-arg REPO_ROOT=${REPO_ROOT} \
    --build-arg CMAKE_BUILD_PARALLEL_LEVEL=6
"

docker build \
    -t ${REPO_ROOT}/soniks-client:${TAG} \
    ../addons "$@"

